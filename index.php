<?php include_once('/includes/init.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
	<title>Giving Joy</title>	
	<?php include_once('/includes/css.php'); ?>
</head>
<body>
	<?php include_once('/includes/menu.php'); ?>
	<div class="container">
		<div class="front-page-container row-fluid">
			<h1 class="center">We're changing culture, society, &amp; the world.</h1>
			<p class="lead">Join our movement of giving 2.0</p>
		</div>

		<div id="buttons">
			<div class="row-fluid">
				<div class="span4 align-center">
					<a class="btn btn-primary btn-large" href="/buy-a-gift-card.php">I want to give someone a Gift Card</a>
				</div>
				<div class="span4 align-center">
					<a class="btn btn-primary btn-large" href="/buy-a-gift-card.php">I want to use a Gift Card</a>
				</div>
				<div class="span4 align-center">
					<a class="btn btn-primary btn-large" href="/buy-a-gift-card.php">I want to add a Project</a>
				</div>
			</div>
		</div>

		<div id="faces">
			<div class="row-fluid">
				<div class="face-row span12">

				</div>
			</div>
			<div class="row-fluid">
				<div class="face-column">

				</div>
				<div id="how_button_holder" class="face-column">
					<button type="button" class="btn btn-success btn-large">How?</button>
				</div>
				<div class="face-column">

				</div>
			</div>
			<div class="row-fluid">
				<div class="face-row span12">

				</div>
			</div>
		</div>
	</div>
	<?php include_once('/includes/js.php'); ?>
</body>

</html>